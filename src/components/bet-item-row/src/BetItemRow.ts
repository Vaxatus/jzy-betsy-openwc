import { html, css, LitElement, property } from 'lit-element';
import { BetTeam } from '../../types';

export class BetItemRow extends LitElement {
  @property({type: Object }) home = {} as BetTeam;
  @property({type: Object}) away = {} as BetTeam;
  @property({type: Number}) draw = null;

  render() {
    const { home: { name:home_name, win:home_win }, away: { name:away_name, win:away_win }, draw }  = this

    return html`
      <li>
        <div>${home_name} - ${away_name}</div>
        <div>${home_win}</div>
        <div>${draw}</div>
        <div>${away_win}</div>
      </li>
    `;
  }
}
