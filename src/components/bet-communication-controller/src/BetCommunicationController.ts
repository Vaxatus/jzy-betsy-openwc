import { html, css, LitElement, property } from 'lit-element';
import io from 'socket.io-client';
import { fromEvent, Observable } from 'rxjs';
// eslint-disable-next-line import/extensions
import { BetItem, Bets } from '../../types';

const config = {
  url: "localhost:3000"
}

export class BetCommunicationController extends LitElement {

  private socket: SocketIOClient.Socket = {} as SocketIOClient.Socket;
  private bets: Bets = {} as Bets

  constructor() {
    super();
    this.socket = io(config.url);
  }

  onReceive(): Observable<BetItem> {
    return fromEvent(this.socket, 'bet-updated');
  }

  disconnectedCallback(): void {
    super.disconnectedCallback();
    this.socket.disconnect();
  }

  firstUpdated () {
    const observable = this.onReceive();
    observable.subscribe((item: BetItem) => {
      const bets = this.bets.items;
      bets.push(item);
    });
  }

  render() {
    return html``;
  }
}

